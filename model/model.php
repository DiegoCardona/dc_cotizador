<?php 

require_once(dirname( __FILE__ ).'/../../../../wp-config.php');
require_once(dirname( __FILE__ ).'/../../../../wp-load.php');

class Cotizador{

	public function get_all_cotization_rules(){
		global $wpdb;
		$results = $wpdb->get_results( 'SELECT * FROM wp_cotization_rules ', OBJECT );
		return $results;		
	}

	public function get_all_cotization_type(){
		global $wpdb;
		$results = $wpdb->get_results( 'SELECT distinct `type` FROM wp_cotization_rules  ', OBJECT );
		return $results;		
	}

	public function get_all_cotization_from(){
		global $wpdb;
		$results = $wpdb->get_results( 'SELECT distinct `from`, `type`  FROM wp_cotization_rules  ', OBJECT );
		return $results;		
	}

	public function get_all_cotization_only_from(){
		global $wpdb;
		$results = $wpdb->get_results( 'SELECT distinct `from`  FROM wp_cotization_rules  ', OBJECT );
		return $results;		
	}

	public function get_all_cotization_region(){
		global $wpdb;
		$results = $wpdb->get_results( 'SELECT distinct `region`, `from`, `type` FROM wp_cotization_rules  ', OBJECT );
		return $results;		
	}

	public function get_all_cotization_only_region(){
		global $wpdb;
		$results = $wpdb->get_results( 'SELECT distinct `region` FROM wp_cotization_rules  ', OBJECT );
		return $results;		
	}

	public function get_all_cotization_to(){
		global $wpdb;
		$results = $wpdb->get_results( 'SELECT distinct  `to`,`to_father`  FROM wp_cotization_rules  ORDER BY `to` ASC ', OBJECT );
		return $results;		
	}

	public function get_cotization_to($type, $from, $region){
		global $wpdb;
		$results = $wpdb->get_results( $wpdb->prepare( "SELECT distinct  `to`,`to_father`  FROM wp_cotization_rules WHERE  `from` = '%s' and  `region` = '%s' and  `type` = '%s' ORDER BY `to` ASC ", $from, $region, $type ), OBJECT );
		
		return $results;		
	}

	public function get_all_cotization_only_to(){
		global $wpdb;
		$results = $wpdb->get_results( 'SELECT distinct  `to`  FROM wp_cotization_rules  ORDER BY `to` ASC ', OBJECT );
		return $results;		
	}

	public function getCotizationRule($from, $to, $type){
		global $wpdb;

		$results = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM wp_cotization_rules 
					WHERE  `from` = '%s' and  `to` = '%s' and  `type` = '%s' ", $from, $to, $type ), OBJECT );			
		if(count($results) == 0){
			$results = "ER";
		}
		return $results[0];
	}

	public function insert_cotization_rule($rule, $reg){

		if(empty($rule['to'])){
			return array('status' => 'ER', 'message'=>'El destino no puede ser vacio', 'from'=>$rule['from'], 'to'=>$rule['to'], 'reg' => $reg);
		}
		if(empty($rule['from'])){
			return array('status' => 'ER', 'message'=>'El origen no puede ser vacio', 'from'=>$rule['from'], 'to'=>$rule['to'], 'reg' => $reg);
		}
		if(is_integer($rule['value'])){
			return array('status' => 'ER', 'message'=>'El valor debe ser un numero entero sin formato', 'from'=>$rule['from'], 'to'=>$rule['to'], 'reg' => $reg);
		}

		

		if($rule['add_value_per_kg'] == "" ){
			$rule['add_value_per_kg'] = 0;
		}
		if(!is_numeric($rule['add_value_per_kg'])){
			return array('status' => 'ER', 'message'=>'El valor adicional por kilo debe ser un numero entero sin formato', 'from'=>$rule['from'], 'to'=>$rule['to'], 'reg' => $reg);
		}
		
		global $wpdb;
		$result = $wpdb->insert('wp_cotization_rules', array('from' => trim($rule['from']),
																'region' => trim($rule['region']),
																'to' => trim($rule['to']),
																'type' => trim($rule['type']),
																'value' => trim($rule['value']),
																'add_value_per_kg' => trim($rule['add_value_per_kg']),
																'departures' => trim($rule['departures']),
																'max_time' => trim($rule['max_time']),
																'to_father' => 'region:'.trim($rule['region']).',from:'.trim($rule['from']).',type:'.trim($rule['type']),
															));
		if(!$result){
			return array('status' => 'ER', 'message'=>'no se ha podido crear la regla de cotizacion', 'from'=>$rule['from'], 'to'=>$rule['to'], 'reg' => $reg);
		}
		return array('status' => 'OK');
	}

	public function delete_all_cotization_rules(){
		global $wpdb;
		$delete = $wpdb->query("TRUNCATE TABLE `wp_cotization_rules`");
	}

	public function get_all_parameters(){
		global $wpdb;
		$results = $wpdb->get_results( 'SELECT * FROM wp_cotization_parameters ', OBJECT );
		
		return $results;		
	}

	public function insert_parameter($parameter, $reg){
		
		global $wpdb;
		$result = $wpdb->insert('wp_cotization_parameters', array('name' => $parameter['name'], 'key'=>$parameter['key'], 'value'=>$parameter['value']));
		if(!$result){
			return array('status' => 'ER', 'message'=>'no se ha podido crear el parametro', 'key'=>$parameter['key'], 'reg' => $reg);
		}
		return array('status' => 'OK');
	}

	public function delete_all_paramteres(){
		global $wpdb;
		$delete = $wpdb->query("TRUNCATE TABLE `wp_cotization_parameters`");
	}

	public function getParameter($key){
		global $wpdb;

		$results = $wpdb->get_results( $wpdb->prepare( "SELECT `value` FROM wp_cotization_parameters 
					WHERE  `key` = '%s' ", $key ), OBJECT );			
		
		if(count($results) == 0){
			$results = array("status" => 'ER', 'message' => 'no se pudo recueperar el parametro' );
		}
		return $results[0]->value;
	}

}

?>