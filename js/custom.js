// /* dc_cotizador js */

var global_filter = {region:''};

jQuery(document).ready(function(){
	jQuery('.dc_cotizar').on('click', function(){
		if(dc_validar()){
			dc_cotizar();
		}
	})	

	dc_events();


});

var dc_validar = function(){
	var peso  = 0;
	if(jQuery(".dc_peso_kg").is(':visible')){
		peso = jQuery(".dc_peso_kg").val()
	}else{
		peso = 2;
	}
	
	global_filter.weight = peso;
	global_filter.type = jQuery('#dc_type').val();
	global_filter.from = jQuery('#dc_from').val();
	global_filter.region = jQuery('#dc_region').val();
	global_filter.to = jQuery('#dc_to').val();


	var valor_asegurar  = 0;
	if(jQuery(".dc_insurance_value").is(':visible')){
		valor_asegurar = jQuery(".dc_insurance_value").val()
	}
	global_filter.insurance_value = valor_asegurar;

	var msg = "";
	//type
	if(global_filter.type == ""){
		msg += "- Debe seleccionar un tipo de Paquete \n";
	}
	//from
	if(global_filter.from == "" ){
		msg += "- Debe seleccionar una ciudad origen \n";
	}
	//region
	if(global_filter.region == "" ){
		msg += "- Debe seleccionar una región \n";
	}
	//to
	if(global_filter.to == "" ){
		msg += "- Debe seleccionar una ciudad de destino \n";
	}

	//weight
	if(global_filter.weight == "" || global_filter.weight == 0){
		msg += "- Debe ingresar un peso en kilogramos \n";
	}

	if(msg !=  ""){
		alert(msg);
		return false;
	}

	return true;
}

var dc_cotizar = function(){
	jQuery('.dc_form_section').css('display', 'none');
	jQuery('.dc_result_section').css('display', 'none');
	jQuery('.dc_loading_section').css('display', '');

	dc_cotizar_service();
	
	
}

var dc_before_change_to_service = function(){

	var postData = {type: jQuery('#dc_type').val(), from: jQuery('#dc_from').val()
					, region: jQuery('#dc_region').val()
					, action: 'getTo'};
	var _url = jQuery("#dc_cotizar_url").val() + "/dc_cotizador/controller/api.php";

	
	jQuery.ajax({
	        type: "GET",
	        dataType: "json",
	        url: _url,
	        data: postData,
	        success: function(data){
	        	dc_after_change_to_service(data);

	        },
	        error: function(e){
	            console.log(e.message);
	        }
	});
}


var dc_after_change_to_service = function(data){
	console.log(JSON.stringify(data));
	var elements_to_show = [];

	for (var i = 0; i < data.to.length; i++) {
		elements_to_show[i] = data.to[i].to;
	}
	 

	jQuery('#select2-dc_to-results').find('li').each(function(){
        if(elements_to_show.includes(jQuery(this).html().trim())){
          jQuery(this).css('display','');
        }else{
          jQuery(this).css('display','none');
        }
    });

}




var dc_cotizar_service = function(){

	var postData = {type: global_filter.type, from: global_filter.from
					, to: global_filter.to, weight: global_filter.weight
					, action: 'cotizar'
					, insurance_value: global_filter.insurance_value};
	var _url = jQuery("#dc_cotizar_url").val() + "/dc_cotizador/controller/api.php";
	
	jQuery.ajax({
	        type: "POST",
	        dataType: "json",
	        url: _url,
	        data: postData,
	        success: function(data){
	        	if(data.status == 'OK'){
	        		dc_result(data);
	        	}else{
	        		alert('En el momento no se puede realizar la cotización intente mas tarde')
	        		jQuery('.dc_form_section').css('display', '');
					jQuery('.dc_loading_section').css('display', 'none');
					jQuery('.dc_result_section').css('display', 'none');

	        	}
	        },
	        error: function(e){
	            console.log(e.message);
	        }
	});
}

var dc_formatNumberPesos = function (n) {

    var nfObject = new Intl.NumberFormat('en-US')
    return nfObject.format(n)
}

var dc_result = function(data){
	jQuery('.dc_cotizador_from').html(global_filter.from);
	jQuery('.dc_cotizador_to').html(global_filter.to);
	jQuery('.dc_cotizador_type').html(global_filter.type);
	jQuery('.dc_cotizador_kg').html(global_filter.weight);

	var precio = data.pricing;
	var seguro = data.insurance_pricing;
	var total  = data.total;



	jQuery('.dc_cotizador_precio').html("$" + dc_formatNumberPesos(precio));
	jQuery('.dc_cotizador_seguro').html("$" + dc_formatNumberPesos(seguro));
	jQuery('.dc_cotizador_total').html("$" + dc_formatNumberPesos(total));

	if(jQuery("#dc_lock_weight").val().includes(global_filter.type)){
		jQuery(".dc_row_peso_table").css('display', 'none');
	}else{
		jQuery(".dc_row_peso_table").css('display', '');
	}

	jQuery('.dc_form_section').css('display', 'none');
	jQuery('.dc_loading_section').css('display', 'none');
	jQuery('.dc_result_section').css('display', '');
}



var dc_events = function(){

	setTimeout(function(){
		jQuery('#select2-dc_from-container').on('click', function(){
			var expanded = jQuery(this).parent().attr("aria-expanded");
			if(expanded){
				var elements_to_show = [];

				 jQuery('#dc_from').find('option').each(function(){
		            var text_father = jQuery(this).data('father');
		            if(typeof text_father !== "undefined"){
		              var par = text_father.split("|");
		              
		              for (var i = 0; i < par.length; i++) {
		                if(typeof par[i] !== "undefined" && par[i].indexOf(jQuery('#dc_type').val()) != -1){
		                  elements_to_show.push(jQuery(this).html());
		                }
		                
		              }
		            }
		        })

				jQuery('#select2-dc_from-results').find('li').each(function(){
	                if(elements_to_show.includes(jQuery(this).html())){
	                  jQuery(this).css('display','');
	                }else{
	                  jQuery(this).css('display','none');
	                }
		        });
			}
		})


		jQuery('#select2-dc_region-container').on('click', function(){
			var expanded = jQuery(this).parent().attr("aria-expanded");
			if(expanded){
				
				var elements_to_show = [];

				 jQuery('#dc_region').find('option').each(function(){
		            var text_father = jQuery(this).data('father');
		            if(typeof text_father !== "undefined"){
			            var par = text_father.split("|");
			            for (var i = 0; i < par.length; i++) {

			              if(typeof par[i] !== "undefined" && par[i].indexOf(jQuery('#dc_type').val()) != -1
                            && par[i].indexOf(jQuery('#dc_from').val()) != -1
                            ){
			                  elements_to_show.push(jQuery(this).html());
			                }
			            }
		                
		              }
		        })


				jQuery('#select2-dc_region-results').find('li').each(function(){
	                if(elements_to_show.includes(jQuery(this).html())){
	                  jQuery(this).css('display','');
	                }else{
	                  jQuery(this).css('display','none');
	                }
		        });
			}
		})


		jQuery('#select2-dc_to-container').on('click', function(){
			var expanded = jQuery(this).parent().attr("aria-expanded");
			if(expanded){
				jQuery('#select2-dc_to-results').find('li').each(function(){
		            jQuery(this).css('display','none');
		        });
				
				dc_before_change_to_service();
				
			}
		})



	},1000);

	/** LIMPIANDO CAMPOS **/
	jQuery('#dc_type').on('change', function(){
		jQuery('#dc_from').prop('selectedIndex',0);
		jQuery('#dc_from').parent().find('.select2-selection__rendered').html("Seleccione...");

		jQuery('#dc_region').prop('selectedIndex',0);
		jQuery('#dc_region').parent().find('.select2-selection__rendered').html("Seleccione...");

		if(jQuery("#dc_lock_weight").val().includes(jQuery(this).val())){
			jQuery(".dc_row_peso").css('display', 'none');
		}else{
			jQuery(".dc_row_peso").css('display', '');
		}
	});

	jQuery('#dc_from').on('change', function(){
		jQuery('#dc_region').prop('selectedIndex',0);
		jQuery('#dc_region').parent().find('.select2-selection__rendered').html("Seleccione...");
	});



}



