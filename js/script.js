jQuery(document).ready(function(){

	jQuery('#dc_cotizador_descargar').on('click', function(){
		dc_cotizador_descargar();
	});

	jQuery('#dc_cotizador_cargar').on('click', function(){
		console.log('descargando');
		dc_cotizador_cargar();
	});

	jQuery('.loadResultClose').on('click', function(){
		var modal = document.getElementById("loadResult");
		modal.style.display = "none";
		window.location.reload();
	});

});

/***************************** PROCESAMIENTO DE ARCHIVO **********************************/

var dc_cotizador_descargar = function(){
	downloadAjax();
}

var dc_cotizador_cargar = function(){
	if(confirm('¿esta seguro de cargar la nueva informacion?')){
		uploadAjax(document.getElementById('dc_cotizador_archivo'));
	}
}

/*************************** PROCESAMIENTO DE CAMPANNA ********************************/

var dc_cotizador_get_all_campanna = function(){
	
}

var dc_cotizador_get_campanna = function(){

}

var dc_cotizador_edit_campanna = function(){
	
}

var dc_cotizador_save_campanna = function(){
	
}

/******* GENERIC ********************/


var callback_error = function(data){
	alert('error '+JSON.stringify(data));
}

var callback_success_default = function(data){
	alert('error '+JSON.stringify(data));
}

var ajax_post = function(postData, callback, callback_error){
	var _url = controller;
	show_loading();
	jQuery.ajax({
	        type: "POST",
	        dataType: "json",
	        url: _url,
	        data: postData,
	        success: function(data){
	        	if(data.status == 'OK'){
	        		window[callback](data);	
	        	}else{
	        		window[callback_error](data.message);
	        	}
	        	hide_loading();
	        },
	        error: function(e){
	        	hide_loading();
	            console.log(e.message);
	            callback_error(e.message);
	        }
	});
}

var show_loading = function(){
	//jQuery('#rtb_loading').css('display','');
}

var hide_loading = function(){
	//jQuery('#rtb_loading').css('display','none');	
}


var uploadAjax = function(inputFile){
	//console.log('antes');
	var file = inputFile.files[0];
	var data = new FormData();
	data.append('archivo',file);
	data.append('action', jQuery('#dc_cotizador_action').val());
	//console.log('durante');
	var _url = jQuery('#dc_cotizador_plugin_path').val() + '/dc_cotizador/controller/controller.php';
	//console.log(_url);
	jQuery.ajax({
		url:_url,
		type:'POST',
		contentType:false,
		data:data,
		processData:false,
		cache:false,
		success: function(data){
        	if(data.status == 'OK'){
        		var modal = document.getElementById("loadResult");
        		modal.style.display = "block";
        		console.log(data.log_cotization)
        		console.log(data.log_cotization.length)
        		if(data.log_cotization.length == 0 && data.log_parameters.length == 0){
        			jQuery('.modalMessage').html("con exito");
        		}else{
        			jQuery('.modalMessage').html("con errores");
        			if(data.log_cotization.length != 0){
        				var table = "<table border=1 style='border-collapse: collapse;'>";
        				table += "<tr><td><b>fila</b></td><td><b>Origen</b></td><td><b>Destino</b></td><td><b>Mensaje</b></td>";
        				for (var i = 0; i < data.log_cotization.length; i++) {
        					table += "<tr><td>"+data.log_cotization[i].reg+"</td>";
        					table += "<td>"+data.log_cotization[i].from+"</td>";
        					table += "<td>"+data.log_cotization[i].to+"</td>";
        					table += "<td>"+data.log_cotization[i].message+"</td>";
        					table += "</tr>";
        				}
        				table += "</table>";
        				jQuery('.modalCotization').html(table);
        			}
        			if(data.log_parameters.length == 0){
        				
        			}
        		}
        		//alert(data.message);
        	}else{
        		alert(data.message);
        	}
        	//alert(JSON.stringify(data));
        },
        error: function(e){
        	
        }
	});

}

var downloadAjax = function(){
	var data = new FormData();
	data.append('action', 'descargando');
	var _url = jQuery('#dc_cotizador_plugin_path').val() + '/dc_cotizador/controller/controller.php';
	jQuery.ajax({
		url:_url,
		type:'POST',
		contentType:false,
		data:data,
		processData:false,
		cache:false,
		success: function(data){
        	//alert(JSON.stringify(data));
        	if(data.status == 'OK'){
        		//window.open(jQuery('#dc_cotizador_plugin_path').val() + '/dc_cotizador/temp/'+data.archivo, '', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
        		window.location.href = jQuery('#dc_cotizador_plugin_path').val() + '/dc_cotizador/temp/'+data.archivo;

        	}else{
        		alert('ha ocurrido un problema descargando el archivo');
        	}
        },
        error: function(e){
        	
        }
	});

}




