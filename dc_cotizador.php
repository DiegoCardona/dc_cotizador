<?php



require_once(dirname( __FILE__ ).'/../../../wp-config.php');
require_once(dirname( __FILE__ ).'/../../../wp-load.php');
/*
* Plugin Name: dc_cotizador
* Description: Desarrollado para cotizar los precios de envio del sitio basado en los precios subidos desde excel, este plugin trabaja de la mano del plugin bt_cost_calculador que hace la parte grafica
* Version: 1.0
* Author: DiegoCardona(diego0123@gmail.com)
* Autor Uri: http://thedeveloper.co
*/

/* helpers */
function get_url(){
	$plugin_dir = plugin_dir_path( __FILE__ );
	return plugin_dir_path( __FILE__ );
}

// register jquery and style on initialization
add_action('init', 'register_script');
function register_script() {
    wp_register_script( 'custom_jquery', plugins_url('/js/custom.js', __FILE__), array('jquery'), '2.5.1' );

    wp_register_style( 'new_style', plugins_url('/css/style.css', __FILE__), false, '1.0.0', 'all');
}

// use the registered jquery and style above
add_action('wp_enqueue_scripts', 'enqueue_style');

function enqueue_style(){
   wp_enqueue_script('custom_jquery');

   wp_enqueue_style( 'new_style' );
}

/*******************/

function rtb_maps_js_include()
{
	wp_enqueue_script('script.js', WP_PLUGIN_URL. '/dc_cotizador/js/script.js' );
}

add_action( 'admin_enqueue_scripts', 'rtb_maps_js_include' );

add_action( 'admin_menu', 'dc_cotizador_custom_admin_menu' );
 
function dc_cotizador_custom_admin_menu() {
    add_options_page(
        'Cotizador',
        'Cotizador',
        'manage_options',
        'dc-cotizador-plugin',
        'dc_cotizador_main_page'
    );
}

function dc_cotizador_main_page() {
	define('DC_COTIZADOR_PLUGUIN_NAME','dc_cotizador');
	include_once get_url() .'view/main_page.php';    
}


function create_plugin_database_table()
{
    error_log('creating plugin');
    global $table_prefix, $wpdb;

    $tblname = 'cotization_rules';
    $wp_track_table = $table_prefix . "$tblname";

    

    if($wpdb->get_var( "show tables like '$wp_track_table'" ) != $wp_track_table) 
    {

        $sql = "CREATE TABLE `". $wp_track_table . "` ( ";
        $sql .= "  `id`  int(11)   NOT NULL auto_increment, ";
        $sql .= "  `from`  varchar(128)   NOT NULL, ";
        $sql .= "  `region`  varchar(128)   , ";
        $sql .= "  `to`  varchar(128)   NOT NULL, ";
        $sql .= "  `type`  varchar(128)   NOT NULL, ";
        $sql .= "  `value`  varchar(128)   NOT NULL, ";
        $sql .= "  `add_value_per_kg`  varchar(128), ";
        $sql .= "  `departures`  varchar(128), ";
        $sql .= "  `max_time`  varchar(128)   NOT NULL, ";
        $sql .= "  `to_father`  varchar(528) , ";
        $sql .= "  PRIMARY KEY `order_id` (`id`) "; 
        $sql .= ") ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ; ";
        error_log('creando '.$sql);
        require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
        dbDelta($sql);
    }

    $tblname = 'cotization_parameters';
    $wp_track_table = $table_prefix . "$tblname";

    

    if($wpdb->get_var( "show tables like '$wp_track_table'" ) != $wp_track_table) 
    {

        $sql = "CREATE TABLE `". $wp_track_table . "` ( ";
        $sql .= "  `id`  int(11)   NOT NULL auto_increment, ";
        $sql .= "  `name`  varchar(1280)   NOT NULL, ";
        $sql .= "  `key`  varchar(1280)   NOT NULL, ";
        $sql .= "  `value`  varchar(1280)   NOT NULL, ";
        $sql .= "  PRIMARY KEY `order_id` (`id`) "; 
        $sql .= ") ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ; ";
        error_log('creando '.$sql);
        require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
        dbDelta($sql);
    }
}

 register_activation_hook( __FILE__, 'create_plugin_database_table' );

function dc_cotizador_prepare_init_data(){
    
    include(dirname( __FILE__ ).'/controller/form.php');
    $data['WP_PLUGIN_URL'] = WP_PLUGIN_URL;
    $opts = array(
      'http'=>array(
        'method'=>"POST",
        'header'=>"Accept-language: en\r\n" .
                  "Cookie: foo=bar\r\n" .  // check function.stream-context-create on php.net
                  "User-Agent: Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.102011-10-16 20:23:10\r\n",
        'content' => http_build_query($data)
      )
    );

    return stream_context_create($opts);
}

function dc_cotizador_prepare_init_data_v2($url){
    
    include(dirname( __FILE__ ).'/controller/form.php');
    $data['WP_PLUGIN_URL'] = WP_PLUGIN_URL;

   $fields_string = http_build_query($data);
    $ch = curl_init($url); 
    curl_setopt($ch, CURLOPT_POST, true); 
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
    $output = curl_exec ($ch); 
    curl_close ($ch); 
    
    return $output;
}

function dc_cotizador_prepare_init_data_v3(){
    
    include(dirname( __FILE__ ).'/controller/form.php');
    $data['WP_PLUGIN_URL'] = WP_PLUGIN_URL;
    return $data;
}

// function that runs when shortcode is called
function dc_cotizador_shortcode_desktop() { 
    return dc_cotizador_prepare_init_data_v2(WP_PLUGIN_URL. '/dc_cotizador/view/cotizador_desktop.php');

} 
// register shortcode
add_shortcode('dc_cotizador_shortcode_desktop', 'dc_cotizador_shortcode_desktop');

function dc_cotizador_shortcode_mobile() { 
      $data = dc_cotizador_prepare_init_data_v3();
    $res = '<html>...</html>';
    return $res;
} 

add_shortcode('dc-cotizador-shortcode', 'dc_cotizador_shortcode_mobile');

?>