<?php 

header('Access-Control-Allow-Origin: *');
header('content-type: application/json; charset=utf-8');


require_once '../model/model.php';

if(!isset($_POST['action']) && !isset($_GET['action'])){
	$resultado = array('status'=>'ERROR', 'message' =>'ha ocurrido un error');
	exit();
}

if(isset($_POST['action'])){
	$action = $_POST['action'];	
}else{
	$action = $_GET['action'];
}



switch ($action) {
	case 'getTo':
			$type 				= $_GET['type'];
			$from 				= $_GET['from'];
			$region 			= $_GET['region'];
			$cotizador = new Cotizador();
			$to = $cotizador->get_cotization_to($type, $from, $region);
			$result = array('status' => 'OK','to' => $to);
			echo json_encode($result);
		break;
	case 'cotizar':


		$type 				= $_POST['type'];
		$from 				= $_POST['from'];
		$to 				= $_POST['to'];
		$weight 			= $_POST['weight'];
		$insurance_value 	= $_POST['insurance_value'];
		
		$cotizador = new Cotizador();
		$seguro_valor_minimo = $cotizador->getParameter('SEG_MIN_VAL');
		$seguro_porcentaje   = $cotizador->getParameter('SEG_PORCENTAJE');
		$tiempo_entrega      = $cotizador->getParameter('TIEMPO_ENTREGA');
		$dias_entrega        = $cotizador->getParameter('DIAS_ENTREGA');
		$max_to_extra        = $cotizador->getParameter('MAX_TO_EXTRA');
		$seg_def_val         = $cotizador->getParameter('SEG_DEF_VAL');

		$rule = $cotizador->getCotizationRule($from, $to, $type);


		$value = $rule->value;
		$add_value_per_kg = $rule->add_value_per_kg;

		/*analizando valor extra y valor normal*/
		$weight_extra = 0;
		if($weight > $max_to_extra){
			$weight_extra 	= $weight - $max_to_extra;
		}
		$pricing = $value + $weight_extra * $add_value_per_kg;

		/* analizando el seguro */
		$insurance_pricing = 0;

		error_log($insurance_value);
		error_log($seguro_valor_minimo);
		if($insurance_value >= $seguro_valor_minimo){
			$insurance_pricing = $seguro_porcentaje /100 * $insurance_value;
		}else{
			$insurance_pricing = $seg_def_val;
		}
		

		$total = $pricing + $insurance_pricing;


		$result = array('status' => 'OK','pricing' => $pricing, 'insurance_pricing' => $insurance_pricing, 'total' => $total);
		echo json_encode($result);
		break;
	
	default:
		# code...
		break;
}


?>