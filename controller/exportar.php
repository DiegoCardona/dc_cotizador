<?php 

function exportar_excel($data_cotization, $parameters){

	
	$objPHPExcel = new PHPExcel();
	   
   //Informacion del excel
   $objPHPExcel->
    getProperties()
        ->setCreator("thedeveloper.co")
        ->setLastModifiedBy("thedeveloper.co")
        ->setTitle("DC Cotizador")
        ->setSubject("DC Cotizador")
        ->setDescription("DC Cotizador")
        ->setKeywords("DC Cotizador")
        ->setCategory("DC Cotizador");    
	   $sheet = 0;
	   $i = 1;  
	   $objPHPExcel->setActiveSheetIndex($sheet)->setTitle("Cotizador"); 
	   $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('A'.$i, 'ORIGEN');
	   $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('B'.$i, 'REGION');
	   $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('C'.$i, 'DESTINO'); 
	   $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('D'.$i, 'TIPO DE PAQUETE');
	   $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('E'.$i, 'VALOR');
	   $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('F'.$i, 'VALOR KILO ADICIONAL');
	   $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('G'.$i, 'SALIDAS');
	   $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('H'.$i, 'TIEMPO MAXIMO DE ENTREGA');

	  

	   foreach($data_cotization as $cotice){
	      $i++; 
	      $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('A'.$i, $cotice->from);
	      $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('B'.$i, $cotice->region);
	      $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('C'.$i, $cotice->to);
	      $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('D'.$i, $cotice->type);
	      $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('E'.$i, $cotice->value);
	      $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('F'.$i, $cotice->add_value_per_kg);
	      $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('G'.$i, $cotice->departures);
	      $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('H'.$i, $cotice->max_time);
	      
	   }

	   $sheet = 1;
	   $i = 1;  

	   $objWorkSheet = $objPHPExcel->createSheet($sheet);
	   $objPHPExcel->setActiveSheetIndex($sheet)->setTitle("Parametros"); 
	   $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('A'.$i, 'NOMBRE');
	   $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('B'.$i, 'CLAVE');
	   $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('C'.$i, 'VALOR');
	 

	   foreach($parameters as $parameter){
	      $i++; 
	      $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('A'.$i, $parameter->name);
	      $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('B'.$i, $parameter->key);
	      $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('C'.$i, $parameter->value);
	      
	   }
	 	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
		$file = md5(date("YmdHis")).'.xlsx';
	 	$objWriter->save('../temp/'. $file);
	 	return $file;
}

function cargar_desde_excel(&$data_cotization, &$parameters, $archivo){

	$objPHPExcel = PHPExcel_IOFactory::load($archivo);
	$objPHPExcel->setActiveSheetIndex(0);
	//Obtengo el numero de filas del archivo
	$numRows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
	$informacion = array();
	for ($i = 2; $i <= $numRows; $i++) {
	    $data_cotization[] = array(
	        'from' => $objPHPExcel->getActiveSheet()->getCell('A'.$i)->getCalculatedValue(),
	        'region' => $objPHPExcel->getActiveSheet()->getCell('B'.$i)->getCalculatedValue(),
	        'to' => $objPHPExcel->getActiveSheet()->getCell('C'.$i)->getCalculatedValue(),
	        'type' => $objPHPExcel->getActiveSheet()->getCell('D'.$i)->getCalculatedValue(),
	        'value' => $objPHPExcel->getActiveSheet()->getCell('E'.$i)->getCalculatedValue(),
	        'add_value_per_kg' => $objPHPExcel->getActiveSheet()->getCell('F'.$i)->getCalculatedValue(),
	        'departures' => $objPHPExcel->getActiveSheet()->getCell('G'.$i)->getCalculatedValue(),
	        'max_time' => $objPHPExcel->getActiveSheet()->getCell('H'.$i)->getCalculatedValue()
	    );

	 }

	$objPHPExcel->setActiveSheetIndex(1);
	//Obtengo el numero de filas del archivo
	$numRows = $objPHPExcel->setActiveSheetIndex(1)->getHighestRow();
	$informacion = array();
	for ($i = 2; $i <= $numRows; $i++) {
	    $parameters[] = array(
	        'name' => $objPHPExcel->getActiveSheet()->getCell('A'.$i)->getCalculatedValue(),
	        'key' => $objPHPExcel->getActiveSheet()->getCell('B'.$i)->getCalculatedValue(),
	        'value' => $objPHPExcel->getActiveSheet()->getCell('C'.$i)->getCalculatedValue()
	    );
	 }
}

?>