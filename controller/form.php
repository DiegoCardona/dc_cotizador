<?php 


require_once(dirname( __FILE__ ).'/../../../../wp-config.php');
require_once(dirname( __FILE__ ).'/../../../../wp-load.php');
require_once(dirname( __FILE__ ).'/../model/model.php');



$data = [];
$cotizador = new Cotizador();
$types 	= $cotizador->get_all_cotization_type();


/************ MANEJO DEL FROM ***************/
$from = $cotizador->get_all_cotization_from();
$only_from =  $cotizador->get_all_cotization_only_from();
$last_from = [];
foreach ($only_from as $element) {
	$types_temp = array();
	foreach ($from as $types) {
		if($element->from == $types->from && !in_array($types->type, $types_temp)){
			$types_temp[] = $types->type;
		}
	}
	$last_from[] = array('from' => $element->from, 'types' => implode(',', $types_temp));
	
}


/********** MANEJO DEL REGION **************/

$regions = $cotizador->get_all_cotization_region();
$only_region =  $cotizador->get_all_cotization_only_region();

$result = [];
//types
foreach ($regions as $region) {
	$result[$region->region] = [];
}

foreach ($regions as $region) {
	if(!in_array($region->type, $result[$region->region])){
		$result[$region->region][] = $region->type;
	}
}

$result_final = [];
foreach ($result as $region => $array) {
	$type_temp = [];
	foreach ($array as $key => $type) {
		$from_temp = [];
		foreach ($regions as $region2) {
			if($region == $region2->region && $type == $region2->type){
				if(!in_array($region2->from, $from_temp)){
					$from_temp[] = $region2->from;
				}
			}

		}
		$type_temp[] = $type.':'.implode(',', $from_temp);
	}	

	$result_final[] = array('region' => $region, 'father' => implode('|', $type_temp));
	
}


/************ MANEJO DEL TO *******************/

$to = $cotizador->get_all_cotization_to();
$to_final = [];
foreach ($to as $element) {
	if(isset($to_final[$element->to])){
		$to_final[$element->to] .= '|'.$element->to_father;
	}else{
		$to_final[$element->to] = $element->to_father; 
	}
}

$result_to = [];
foreach ($to_final as $key => $value) {
	$result_to[] = array('to' => $key, 'to_father' => $value);
}


$data['from'] = $last_from;
$data['region'] = $result_final;
$data['to'] 	= $result_to;
$data['type'] 	= $cotizador->get_all_cotization_type();
$data['programar_recogida'] = $cotizador->getParameter('PROGRAMAR_RECOGIDA');
$data['programar_recogida_msg'] = $cotizador->getParameter('PROGRAMAR_RECOGIDA_MENSAJE');
$data['programar_recogida_btn'] = $cotizador->getParameter('PROGRAMAR_RECOGIDA_BTN');
$data['nota_cotizacion'] = $cotizador->getParameter('NOTA_COTIZACION');
$data['lock_weight'] = $cotizador->getParameter('LOCK_WEIGHT');
$data['site_url'] =  get_site_url();



?>