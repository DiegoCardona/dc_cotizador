<?php 

header('content-type: application/json; charset=utf-8');

require_once '../lib/excel/PHPExcel.php';
require_once("../lib/excel/PHPExcel/IOFactory.php");

require_once '../model/model.php';
require_once '../controller/exportar.php';

if(!isset($_POST['action'])){
	$resultado = array('status'=>'ERROR', 'message' =>'ha ocurrido un error');
	echo json_encode($resultado);
	exit();
}

$action = $_POST['action'];

switch ($action) {
	case 'cargando':
		
		$upload_folder ='../temp';
		$nombre_archivo = $_FILES['archivo']['name'];
		$tipo_archivo = $_FILES['archivo']['type'];
		$tamano_archivo = $_FILES['archivo']['size'];
		$tmp_archivo = $_FILES['archivo']['tmp_name'];
		$ext	= pathinfo($nombre_archivo, PATHINFO_EXTENSION);
		$archivador = $upload_folder . '/' . md5(date("YmdHis")) . '.' . $ext;;
		
		if (!move_uploaded_file($tmp_archivo, $archivador)) {
			$return = array('status'=>'ERROR', 'message' =>'ha ocurrido un error al cargar el archivo');
			echo json_encode($return);
			exit();
		}
		
		 cargar_desde_excel($data_cotization, $parameters, $archivador);

		 $cotizador = new Cotizador();
		 $log = array();
		 $cont = 2;
		 $cotizador->delete_all_cotization_rules();
		 foreach ($data_cotization as $cotization) {
		 	$reg = $cotizador->insert_cotization_rule($cotization, $cont);
		 	if($reg['status'] != 'OK'){
		 		$log[] = $reg;
		 	}
		 	$cont++;
		 }
		 
		 $log2 = array();
		 $cont = 2;
		 $cotizador->delete_all_paramteres();
		 foreach ($parameters as $parameter) {
		 	$reg = $cotizador->insert_parameter($parameter, $cont);
		 	if($reg['status'] != 'OK'){
		 		$log2[] = $reg;
		 	}
		 	$cont++;
		 }

		 echo json_encode(array('status'=>'OK', 'message'=>'carga finalizada', 'log_cotization'=>$log, 'log_parameters' => $log2));
		 
		break;
	
	case 'descargando':
		$cotizador = new Cotizador();
		$file = exportar_excel($cotizador->get_all_cotization_rules(), $cotizador->get_all_parameters());		
		echo json_encode(array('status'=>'OK', 'archivo' => $file));
		break;

	default:
		# code...
		break;
}


?>