<style type="text/css">
/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 100000; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
  position: relative;
  background-color: #fefefe;
  margin: auto;
  padding: 0;
  border: 1px solid #888;
  width: 60%;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
  -webkit-animation-name: animatetop;
  -webkit-animation-duration: 0.4s;
  animation-name: animatetop;
  animation-duration: 0.4s
}

/* Add Animation */
@-webkit-keyframes animatetop {
  from {top:-300px; opacity:0} 
  to {top:0; opacity:1}
}

@keyframes animatetop {
  from {top:-300px; opacity:0}
  to {top:0; opacity:1}
}

/* The Close Button */
.close {
  color: #000;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

.modal-header {
  padding: 2px 16px;
  color: white;
}

.modal-body {padding: 2px 16px;}

.modal-footer {
  padding: 2px 16px;
  color: white;
}
</style>

<input type="hidden" id="dc_cotizador_plugin_path" value="<?php echo WP_PLUGIN_URL ?>">
<div class="wrap">
	<div class="card pressthis">
		<h2>Carga Masiva de Precios para cotización</h2>
		<p>
			La dinamica de este modulo es la siguiente: <br>
			<h3>Descargar Archivo</h3>
			<ul>
				<li>1. se da clic en descargar archivo, y lo que hace el sistema es generar un archivo con toda la base de datos de precios configurados con ciudades de origen y destino.</li>
				<li>2. si se requiere realizar alguna modificaci&oacute;n, (agregar, editar o borrar), sobre alguno de los registros o registros nuevos, se realiza directamente en este archivo y luego se procede a cargar el archivo usando la opcion de carga de archivo.</li>
			</ul>

			<h3>Carga de Archivo</h3>
			<ul>
				<li>1. una vez se tengan las modificaciones necesarias sobre el archivo, se procede a cargar dicho archivo(debe estar en el mimsmo formato que fue descargado previamente), y el sistema lo que hace es procesar dicho archivo y dejar los nuevos precios disponibles para ser consultados desde el sitio web cuando se usa la opción de cotización.</li>
			</ul>
		</p>
	</div>

		
	<div class="card">
		<h2>Descargar de Archivo</h2>
		<p>Recuerda que esta es la información completa que se tiene en la base de datos</p>
		<input type="button" id="dc_cotizador_descargar" value="Descargar">
	</div>

	<div class="card">
		<h2>Cargar de Archivo</h2>
		<p>Recuerda que la primera fila de cada hoja del archivo es el ENCABEZADO, adicionalmente no se deben cambiar los nombres a las hojas ni el orden</p>
		<input type="file" name="dc_cotizador_archivo" id="dc_cotizador_archivo" />
		<input type="hidden" name="dc_cotizador_action" id="dc_cotizador_action" value="cargando">
		<input type="button" id="dc_cotizador_cargar" value="Cargar" />
	</div>
</div>


<div id="loadResult" class="modal">

  <div class="modal-content">
    <div class="modal-header">
      <span class="close loadResultClose">&times;</span>
      <h2>La carga ha finalizado <span class="modalMessage"></span></h2>
    </div>
    <div class="modal-body">
      <span class="modalCotization" style="height: 114px;overflow: scroll !important;"></span>
      <span class="modalParameters"></span>
      <br><br><br><br><br><br><br><br><br><br><br><br><br>
    </div>
    <div class="modal-footer">
      
    </div>
  </div>

</div>

