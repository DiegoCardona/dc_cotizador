

<div class="btQuoteBookingWrap dc_form">
    <div >
        <header class="header btClear small btAccentDash btRegularTitle">
            <div class="dash">
                <h4><span class="headline">COTIZACIÓN RÁPIDA</span></h4>
            </div>
            <p class="btSubTitle" style="height: 0px;"></p>
        </header>
        <div
            class="dc_form_section"
        >
            <div class="dc_row">
                <div class="dc_column" style="width:30%">
                    <label>Tipo de paquete</label>
                </div>

                <div class="dc_column" style="width:70%">
                    
                    <select id="dc_type" class="dc_element" data-dc_name="type">
                        <option value="">Seleccione...</option>
                        <?php foreach ($_POST['type'] as $key => $value) { ?>
                            <option value="<?php echo $value['type'] ?>"><?php echo $value['type'] ?></option>    
                         <?php } ?>
                    </select>
                                
                </div>
            </div>
            <div class="dc_row">
                <div class="dc_column" style="width:30%">
                    <label>Origen</label>
                </div>

                <div class="dc_column" style="width:70%">
                    
                    <select id="dc_from" class="dc_element" data-dc_name="from" data-dc_father="type">
                        <option value="">Seleccione...</option>
                        <?php foreach ($_POST['from'] as $key => $value) { ?>
                            <option data-father="<?php echo $value['types'] ?>" value="<?php echo $value['from'] ?>"><?php echo $value['from'] ?></option>    
                         <?php } ?>
                    </select>
                                
                </div>
            </div>
            <div class="dc_row">
                <div class="dc_column" style="width:30%">
                    <label>Region</label>
                </div>

                <div class="dc_column" style="width:70%">
                    
                    <select id="dc_region" class="dc_element" data-dc_name="region" data-dc_father="from">
                        <option value="">Seleccione...</option>
                        <?php foreach ($_POST['region'] as $key => $value) { ?>
                            <option data-father="<?php echo $value['father'] ?>" value="<?php echo $value['region'] ?>"><?php echo $value['region'] ?></option>    
                         <?php } ?>
                    </select>
                                
                </div>
            </div>
            <div class="dc_row">
                <div class="dc_column" style="width:30%">
                    <label>Destino</label>
                </div>

                <div class="dc_column" style="width:70%">
                    
                    <select id="dc_to" class="dc_element" data-dc_name="to" data-dc_father="region">
                        <option value="">Seleccione...</option>
                        <?php foreach ($_POST['to'] as $key => $value) { ?>
                            <option data-father="<?php echo $value['to_father'] ?>" value="<?php echo $value['to'] ?>"><?php echo $value['to'] ?></option>    
                         <?php } ?>
                    </select>
                                
                </div>
            </div>

            <div class="dc_row">
                <div class="dc_column" style="width:30%">
                    <label>Peso(Kg)</label>
                </div>

                <div class="dc_column" style="width:70%">
                    
                    <input type="number" placeholder="Ingrese la cantidad en kilos" class="dc_input dc_peso_kg_<?php echo $version?>" maxlength="3">
                                
                </div>
            </div>


            <div class="dc_row">
                <div class="dc_column" style="width:30%">
                    <label>¿Incluye Seguro?</label>
                </div>

                <div class="dc_column" style="width:70%">
                    
                    <select id="dc_seguro" class="dc_element" data-dc_name="seguro" >
                        <option value="">Seleccione...</option>
                        <option value="S">Si</option>
                        <option value="N">No</option>
                      
                    </select>
                                
                </div>
            </div>

             <div class="dc_row dc_row_valor_asegurar">
                <div class="dc_column" style="width:30%">
                    <label>Valor a asegurar</label>
                </div>

                <div class="dc_column" style="width:70%">
                    
                    <input type="number" placeholder="Valor a asegurar" class="dc_input dc_insurance_value_<?php echo $version?>" maxlength="10">
                                
                </div>
            </div>
             <div class="dc_row">
                <div class="dc_column" style="width:100%; text-align: center;">
                    
                   <div class="btBtn btBtn btnFilled btnIconLeftPosition btnAccentColor btnSmall btnNormal" style="margin-left: 20px;"><a class="dc_cotizar">Cotiza ahora</a></div>
                                
                </div>
            </div>

        </div>
        <div
            class="dc_result_section" style="display: none;"
        >
            <div class="dc_row">
                <div class="dc_column" style="width:100%; height: 250px" >
                   <div class="divTable" style="border: 1px solid #000;" >
                    <div class="divTableBody">
                        <div class="divTableRow">
                            <div class="divTableCell">Tipo de paqute</div>
                            <div class="divTableCell dc_cotizador_type">&nbsp;</div>
                        </div>
                        <div class="divTableRow">
                            <div class="divTableCell">Origen</div>
                            <div class="divTableCell dc_cotizador_from">&nbsp;</div>
                        </div>
                        <div class="divTableRow">
                            <div class="divTableCell">Destino</div>
                            <div class="divTableCell dc_cotizador_to">&nbsp;</div>
                        </div>
                        <div class="divTableRow">
                            <div class="divTableCell">Peso(Kg)</div>
                            <div class="divTableCell dc_cotizador_kg">&nbsp;</div>
                        </div>
                        <div class="divTableRow">
                            <div class="divTableCell">Precio</div>
                            <div class="divTableCell dc_cotizador_precio">&nbsp;</div>
                        </div>
                        <div class="divTableRow">
                            <div class="divTableCell">Valor Seguro</div>
                            <div class="divTableCell dc_cotizador_seguro">&nbsp;</div>
                        </div>
                        <div class="divTableRow">
                            <div class="divTableCell">Precio Total</div>
                            <div class="divTableCell dc_cotizador_total">&nbsp;</div>
                        </div>
                    </div>
                   </div>
                    <!-- DivTable.com -->
                </div>
            </div>
            <div class="dc_row">
                <div class="dc_column" style="width:100%; height: 80px; text-align: center;" >
                     <div class="btBtn btBtn btnFilled btnIconLeftPosition btnAccentColor btnSmall btnNormal" style="margin-left: 20px; background-color: #25d366 !important">
                    <a href="https://api.whatsapp.com/send?phone=<?php echo $_POST['programar_recogida'] ?>&text=<?php echo $_POST['programar_recogida_msg'] ?>" target="_blank"><?php echo $_POST['programar_recogida_btn'] ?></a> 

                    </div> 
                    <div class="btnNormalColor btnSmall btnNormal">
                        <a href="<?php echo $_POST['site_url'] ?>">Cotizar de Nuevo</a>
                    </div>
                    
                </div>

              
            </div>
            <div class="dc_row">
                <div class="dc_column" style="width:100%; height: 80px" >
                    <label><?php echo $_POST['nota_cotizacion'] ?></label>
                </div>

              
            </div>
           

        </div>

        <div
            class="dc_loading_section" style="display: none;"
        >
            <div class="dc_row">
                <div class="dc_column" style="width:100%; height: 270px; text-align: center; position: relative;" >
                    <input type="hidden" id="dc_cotizar_url" value="<?php echo $_POST['WP_PLUGIN_URL'] ?>">
                    <img src="<?php echo $_POST['WP_PLUGIN_URL']. '/dc_cotizador/css/loading.png' ?>" style="top:100px">
                </div>
            </div>
            <div class="dc_row">
                <div class="dc_column" style="width:100%; height: 80px" >
                    <label><?php echo $_POST['nota_cotizacion'] ?></label>
                </div>

              
            </div>
           

        </div>
    </div>

</div>
